import 'package:flutter/material.dart';
import 'package:general_flutter_practice_5/custom_painter/paint_circle.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyCounterPage(),
    );
  }
}

class MyCounterPage extends StatefulWidget {
  @override
  _MyCounterPageState createState() => _MyCounterPageState();
}

class _MyCounterPageState extends State<MyCounterPage> {
  List<int> numbers = List.generate(101, (index) => index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: [
          CustomPaint(
            painter: PaintCircle(),
            size: MediaQuery.of(context).size,
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.3,
            child: SizedBox(
              width: 150,
              height: 50,
              child: RaisedButton(
                  child: Text('Change sort'),
                  onPressed: () {
                    setState(() {
                      numbers = numbers.reversed.toList();
                    });
                  }),
            ),
          ),
          Container(
            width: 150,
            height: MediaQuery.of(context).size.height * 0.25,
            color: Colors.white,
            child: ListView.builder(
              itemCount: numbers.length,
              itemBuilder: (context, index) => Container(
                child: Text(
                  numbers[index].toString(),
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
